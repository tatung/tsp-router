#!/bin/sh
mkdir -p "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/png"
mkdir -p "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/xml"
mkdir -p "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/txt"
cp -f "./Resources/raw/AnnotatorTool.png" "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/png/AnnotatorTool.png"
cp -f "./Resources/raw/AnnotatorTool@2x.png" "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/png/AnnotatorTool@2x.png"
cp -f "./Resources/raw/AnnotatorTool@3to2x.png" "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/png/AnnotatorTool@3to2x.png"
cp -f "./Resources/raw/AnnotatorTool.xml" "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/xml/AnnotatorTool.xml"
cp -f "./Resources/raw/IDToFile.txt" "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/txt/IDToFile.txt"

exit

