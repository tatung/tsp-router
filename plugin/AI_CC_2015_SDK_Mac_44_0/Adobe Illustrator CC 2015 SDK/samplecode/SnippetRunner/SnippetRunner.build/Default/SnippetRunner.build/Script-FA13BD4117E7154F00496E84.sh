#!/bin/sh
mkdir -p "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/png"
mkdir -p "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/txt"
cp -f "./Resources/raw/SnippetRunnerPanelIcon.png" "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/png/SnippetRunnerPanelIcon.png"
cp -f "./Resources/raw/SnippetRunnerPanelRolloverIcon.png" "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/png/SnippetRunnerPanelRolloverIcon.png"
cp -f "./Resources/raw/IDToFile.txt" "${CONFIGURATION_BUILD_DIR}/${PRODUCT_NAME}.aip/Contents/Resources/txt/IDToFile.txt"

exit

