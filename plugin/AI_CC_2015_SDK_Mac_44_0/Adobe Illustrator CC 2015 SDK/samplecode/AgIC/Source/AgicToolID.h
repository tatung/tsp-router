//========================================================================================
//  
//  $File: //ai_stream/rel_19_0/devtech/sdk/public/samplecode/MultiArrowTool/Source/MultiArrowToolID.h $
//
//  $Revision: #1 $
//
//  Copyright 1987 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __AGICTOOLID_H__
#define __AGICTOOLID_H__

#define kPluginName	"AgIC"

#define kCursorID				16000

#define kRedLedIconResID			19002
#define kGreenLedIconResID			19006
#define kBlueLedIconResID		19000
#define kWhiteLedIconResID		19004

#define kRedLedIconDarkResID		19003
#define kGreenLedIconDarkResID		19007
#define kBlueLedIconDarkResID	19001
#define kWhiteLedIconDarkResID	19005

#endif // End MultiArrowToolID.h
