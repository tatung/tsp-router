//========================================================================================
//  
//  $File: //ai_stream/rel_19_0/devtech/sdk/public/samplecode/MultiArrowTool/Source/MultiArrowToolPlugin.cpp $
//
//  $Revision: #1 $
//
//  Copyright 1987 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "IllustratorSDK.h"
#include "AgicToolPlugin.h"
#include "SDKErrors.h"

using namespace ATE;

//#define kHeadArrowTool			"$$$/MulitiArrowTool/Str/HeadArrowTool=Red LED"
//#define kTailArrowTool			"$$$/MulitiArrowTool/Str/TailArrowTool=Green LED"
//#define kBothEndsArrowTool		"$$$/MulitiArrowTool/Str/BothEndsArrowTool=Blue LED"
//#define kStraightLineTool		"$$$/MulitiArrowTool/Str/StraightLineTool=White LED"

#define kRedLed			"$$$/MulitiArrowTool/Str/HeadArrowTool=Red LED"
#define kGreenLed       "$$$/MulitiArrowTool/Str/TailArrowTool=Green LED"
#define kBlueLed		"$$$/MulitiArrowTool/Str/BothEndsArrowTool=Blue LED"
#define kWhiteLed		"$$$/MulitiArrowTool/Str/StraightLineTool=White LED"
#define kPower          "$$$/MulitiArrowTool/Str/PowerTool=Power Pads"

/*
*/
Plugin* AllocatePlugin(SPPluginRef pluginRef)
{
	return new AgicToolPlugin(pluginRef);
}

/*
*/
void FixupReload(Plugin* plugin)
{
	AgicToolPlugin::FixupVTable((AgicToolPlugin*) plugin);
}

/*
*/
AgicToolPlugin::AgicToolPlugin(SPPluginRef pluginRef)
	: Plugin(pluginRef),
    fResourceManagerHandle(NULL),
	fShutdownApplicationNotifier(NULL),
	fNotifySelectionChanged(NULL)
{
	strncpy(fPluginName, kPluginName, kMaxStringLength);
}

/*
*/
ASErr AgicToolPlugin::Message(char* caller, char* selector, void *message)
{
	ASErr error = kNoErr;

	try
	{
		error = Plugin::Message(caller, selector, message);
	}
	catch (ai::Error& ex) {
		error = ex;
	}
	catch (...) {
		error = kCantHappenErr;
	}

	if (error)
	{
		if (error == kUnhandledMsgErr)
		{
			if (strcmp(caller, kCallerAIAnnotation) == 0)
			{
				if (strcmp(selector, kSelectorAIDrawAnnotation) == 0)
				{
					error = DrawAnnotator((AIAnnotatorMessage*)message);
					aisdk::check_ai_error(error);
				}
				else if (strcmp(selector, kSelectorAIInvalAnnotation) == 0)
				{
					error = InvalAnnotator((AIAnnotatorMessage*)message);
					aisdk::check_ai_error(error);
				}				
			}


		}
		else
		{
			Plugin::ReportError(error, caller, selector, message);
		}
	}	
	return error;
}

ASErr AgicToolPlugin::Notify( AINotifierMessage *message )
{
	ASErr error = kNoErr;
	if (message->notifier == fShutdownApplicationNotifier)
	{
		if(fResourceManagerHandle != NULL)
		{
			error = sAIUser->DisposeCursorResourceMgr(fResourceManagerHandle);
			fResourceManagerHandle = NULL;
		}
	}	
	else if (message->notifier == fNotifySelectionChanged) {
			// Get the bounds of the current document view.
			AIRealRect viewBounds = {0, 0, 0, 0};
			error = sAIDocumentView->GetDocumentViewBounds(NULL, &viewBounds);
			aisdk::check_ai_error(error);
			// Invalidate the entire document view bounds.
			error = InvalidateRect(viewBounds);
			aisdk::check_ai_error(error);
	}
	return error;
}
/*
*/
ASErr AgicToolPlugin::StartupPlugin(SPInterfaceMessage* message)
{
	ASErr error = kNoErr;
	ai::int32 pluginOptions = 0;	
	
	error = Plugin::StartupPlugin(message);
	if (error) goto error;
	
	error = sAIPlugin->GetPluginOptions(message->d.self, &pluginOptions);
	if (error) goto error;
	
	error = sAIPlugin->SetPluginOptions(message->d.self, pluginOptions | kPluginWantsResultsAutoSelectedOption );
	if (error) goto error;
	
	error = AddTools(message);
	if (error) goto error;

	error = AddAnnotator(message);
	if (error) goto error;

	error = AddMenus(message);
	if (error) goto error;

	error = AddNotifier(message);
	if (error) goto error; 

error:
	return error;
}

/*
*/
ASErr AgicToolPlugin::GoMenuItem(AIMenuMessage* message)
{
	ASErr error = kNoErr;
	if (message->menuItem == this->fAboutPluginMenu) {
		// Pop this plug-in's about box.
		SDKAboutPluginsHelper aboutPluginsHelper;
		aboutPluginsHelper.PopAboutBox(message, "About AgIC", kSDKDefAboutSDKCompanyPluginsAlertString);
	}	
	return error;
}

/*
*/
ASErr AgicToolPlugin::AddMenus(SPInterfaceMessage* message)
{
	ASErr error = kNoErr;
	// Add a menu item to the About SDK Plug-ins menu group.
	SDKAboutPluginsHelper aboutPluginsHelper;
	error = aboutPluginsHelper.AddAboutPluginsMenuItem(message, 
				kSDKDefAboutSDKCompanyPluginsGroupName, 
				ai::UnicodeString(kSDKDefAboutSDKCompanyPluginsGroupNameString), 
				"AgIC...",
				&fAboutPluginMenu);
	return error;
}

/*
*/
ASErr AgicToolPlugin::AddTools(SPInterfaceMessage* message)
{
	AIErr error = kNoErr;
	AIAddToolData toolData;

	ai::int32			options = kToolWantsToTrackCursorOption;

	char firstToolName[256];	
	ai::UnicodeString toolTitleStr[5];
	toolTitleStr[0] = ai::UnicodeString(ZREF(kRedLed));
	toolTitleStr[1] = ai::UnicodeString(ZREF(kGreenLed));
	toolTitleStr[2] = ai::UnicodeString(ZREF(kBlueLed));
	toolTitleStr[3] = ai::UnicodeString(ZREF(kWhiteLed));
    toolTitleStr[4] = ai::UnicodeString(ZREF(kPower));

	ai::uint32 iconName[5];
	iconName[0] = kRedLedIconResID;
	iconName[1] = kGreenLedIconResID;
	iconName[2] = kBlueLedIconResID;
	iconName[3] = kWhiteLedIconResID;
    iconName[4] = kPowerIconResID;

	ai::uint32 darkIconName[5];
	darkIconName[0] = kRedLedIconDarkResID;
	darkIconName[1] = kGreenLedIconDarkResID;
	darkIconName[2] = kBlueLedIconDarkResID;
	darkIconName[3] = kWhiteLedIconDarkResID;
    darkIconName[4] = kPowerIconDarkResID;

	short i;
	for ( i = 0 ; i < 5 ; i++ ) {
	
		std::string toolTitle = toolTitleStr[i].as_Platform();		
		toolData.title = toolTitle.c_str();
		toolData.tooltip = toolTitle.c_str();
		toolData.darkIconResID = darkIconName[i];
		toolData.normalIconResID = iconName[i];
		if ( i == 0 ) 
		{
            strcpy( firstToolName, toolData.title);
			// New group on tool palette.
			toolData.sameGroupAs = kNoTool;
			// New toolset in new group.
			toolData.sameToolsetAs = kNoTool;
			
		}
		else 
		{
			error = sAITool->GetToolNumberFromName( firstToolName, &toolData.sameGroupAs);			
			error = sAITool->GetToolNumberFromName( firstToolName, &toolData.sameToolsetAs);
		}

		error = sAITool->AddTool( message->d.self, toolData.title, &toolData,
						options, &fToolHandle[i] );
		if (error)
			goto errorTag;
	}

errorTag:
	return error;
}

ASErr AgicToolPlugin::AddNotifier(SPInterfaceMessage *message)
{
	ASErr result = kNoErr;
	try {
		result = sAINotifier->AddNotifier(fPluginRef, "MultiArrowToolPlugin", kAIApplicationShutdownNotifier, &fShutdownApplicationNotifier);
		aisdk::check_ai_error(result);
		result = sAINotifier->AddNotifier(fPluginRef, "MultiArrowToolPlugin", kAIArtSelectionChangedNotifier, &fNotifySelectionChanged);

		aisdk::check_ai_error(result);
	}
	catch (ai::Error& ex) {
		result = ex;
	}
	catch(...)
	{
		result = kCantHappenErr;
	}
	return result;
}

/*
*/
ASErr AgicToolPlugin::TrackToolCursor(AIToolMessage* message)
{
	AIErr error = kNoErr;
	ai::int32 cursorId = kRedLedIconResID;
	
	if ( message->tool == this->fToolHandle[1] )
		cursorId = kGreenLedIconResID;
	else if ( message->tool == this->fToolHandle[2] )
		cursorId = kBlueLedIconResID;
	else if ( message->tool == this->fToolHandle[3] )
		cursorId = kWhiteLedIconResID;
    else if ( message->tool == this->fToolHandle[4])
        cursorId = kPowerIconResID;

	if(sAIUser != NULL)
		error = sAIUser->SetCursor(cursorId, fResourceManagerHandle);

	return error;
}

/*
*/
ASErr AgicToolPlugin::ToolMouseDown(AIToolMessage* message)
{
    ASErr error = kNoErr;
    
    
    
    AIArtHandle frame;
    
    AIArtHandle led;
    
    AIArtHandle anodePad;
    AIArtHandle cathodePad;
    
    AIArtHandle txt = NULL;
    
    AIArtHandle grpLED;
    
    AIArtHandle grpPower;
    
    AIPathSegment ledSegments[4];
    AIPathSegment frameSegments[4];
    AIPathSegment anodePadSegments[4];
    AIPathSegment cathodePadSegments[4];
    
    AIPathSegment powerAnodePadSegments[4];
    AIPathSegment powerCathodePadSegments[4];
    
    AIPathStyle pathStyle;
    AIPathStyleMap pathStyleMap;
    AIDictionaryRef advStrokeParams = NULL;

    
    // we want our initial mouse down to base the drag on later
    this->fStartingPoint = message->cursor;
    
    if(message->tool != this->fToolHandle[4]){
    
        // Create new art, we will fill it with points below.
        error = sAIArt->NewArt(kGroupArt, kPlaceAboveAll, NULL, &grpLED);
        if (error)
            goto error;
        
        error = sAIArt->NewArt(kPathArt, kPlaceInsideOnTop, grpLED, &frame);
        if (error)
            goto error;
        sAIArt->SetArtName(frame, ai::UnicodeString("frame"));
        
        error = sAIArt->NewArt(kPathArt, kPlaceInsideOnTop, grpLED, &led);
        if (error)
            goto error;
        sAIArt->SetArtName(led, ai::UnicodeString("led"));
        
        error = sAIArt->NewArt(kPathArt, kPlaceInsideOnTop, grpLED, &anodePad);
        if (error)
            goto error;
        sAIArt->SetArtName(anodePad, ai::UnicodeString("anodePad"));
        
        error = sAIArt->NewArt(kPathArt, kPlaceInsideOnTop, grpLED, &cathodePad);
        if (error)
            goto error;
        sAIArt->SetArtName(cathodePad, ai::UnicodeString("cathodePad"));
        
        // frame
        error = sAIPath->SetPathSegmentCount(frame, 4);
        
        // 1st point.
        frameSegments[0].p.h = this->fStartingPoint.h;
        frameSegments[0].p.v = this->fStartingPoint.v;
        frameSegments[0].in = frameSegments[0].out = frameSegments[0].p;
        frameSegments[0].corner = true;
        
        // 2nd point
        frameSegments[1].p.h = this->fStartingPoint.h + 10.204;
        frameSegments[1].p.v = this->fStartingPoint.v;
        frameSegments[1].in = frameSegments[1].out = frameSegments[1].p;
        frameSegments[1].corner = true;
        
        // 3rd point
        frameSegments[2].p.h = this->fStartingPoint.h + 10.204;
        frameSegments[2].p.v = this->fStartingPoint.v - 14.74;
        frameSegments[2].in = frameSegments[2].out = frameSegments[2].p;
        frameSegments[2].corner = true;
        
        // 4th point
        frameSegments[3].p.h = this->fStartingPoint.h;
        frameSegments[3].p.v = this->fStartingPoint.v - 14.74;
        frameSegments[3].in = frameSegments[3].out = frameSegments[3].p;
        frameSegments[3].corner = true;
        
        error = sAIPath->SetPathSegments(frame, 0, 4, frameSegments);
        if (error)
            goto error;
        
        error = sAIPath->SetPathClosed(frame, true);
        if (error)
            goto error;
        
        //Draw frame---------------
        // fill and stroke with black; 1 point line
        error = sAIPathStyle->GetCurrentPathStyle(&pathStyle, &pathStyleMap, &advStrokeParams);
        pathStyle.fillPaint = false;
        
        pathStyle.strokePaint = true;
        pathStyle.stroke.color.kind = kGrayColor;
        pathStyle.stroke.color.c.g.gray = kAIRealHalf;
        pathStyle.stroke.width = kAIRealHalf;
        
        error = sAIPathStyle->SetPathStyle(frame, &pathStyle);
        
        // led rect
        error = sAIPath->SetPathSegmentCount(led, 4);
        AIRealPoint fStartingLedPoint;
        fStartingLedPoint.h = this->fStartingPoint.h + 2.834;
        fStartingLedPoint.v = this->fStartingPoint.v - 2.834;
        // 1st point.
        ledSegments[0].p.h = fStartingLedPoint.h;
        ledSegments[0].p.v = fStartingLedPoint.v;
        ledSegments[0].in = ledSegments[0].out = ledSegments[0].p;
        ledSegments[0].corner = true;
        
        // 2nd point 9.071 4.535 pt
        ledSegments[1].p.h = fStartingLedPoint.h + 4.535;
        ledSegments[1].p.v = fStartingLedPoint.v;
        ledSegments[1].in = ledSegments[1].out = ledSegments[1].p;
        ledSegments[1].corner = true;
        
        // 3rd point
        ledSegments[2].p.h = fStartingLedPoint.h + 4.535;
        ledSegments[2].p.v = fStartingLedPoint.v - 9.071;
        ledSegments[2].in = ledSegments[2].out = ledSegments[2].p;
        ledSegments[2].corner = true;
        
        // 4th point
        ledSegments[3].p.h = fStartingLedPoint.h;
        ledSegments[3].p.v = fStartingLedPoint.v - 9.071;
        ledSegments[3].in = ledSegments[3].out = ledSegments[3].p;
        ledSegments[3].corner = true;
        
        error = sAIPath->SetPathSegments(led, 0, 4, ledSegments);
        if (error)
            goto error;
        
        error = sAIPath->SetPathClosed(led, true);
        if (error)
            goto error;
        
        error = sAIPathStyle->GetCurrentPathStyle(&pathStyle, &pathStyleMap, &advStrokeParams);
        
        pathStyle.strokePaint = true;
        pathStyle.stroke.color.kind = kGrayColor;
        pathStyle.stroke.color.c.g.gray = kAIRealOne;
        pathStyle.stroke.width = kAIRealHalf;
        
        // led anodePad
        error = sAIPath->SetPathSegmentCount(anodePad, 4);
        AIRealPoint fStartingAnodePadPoint;
        fStartingAnodePadPoint.h = this->fStartingPoint.h + 2.834;
        fStartingAnodePadPoint.v = this->fStartingPoint.v - 2.834;
        // 1st point.
        anodePadSegments[0].p.h = fStartingAnodePadPoint.h;
        anodePadSegments[0].p.v = fStartingAnodePadPoint.v;
        anodePadSegments[0].in = anodePadSegments[0].out = anodePadSegments[0].p;
        anodePadSegments[0].corner = true;
        
        // 2nd point 9.071 4.535 pt
        anodePadSegments[1].p.h = fStartingAnodePadPoint.h + 4.535;
        anodePadSegments[1].p.v = fStartingAnodePadPoint.v;
        anodePadSegments[1].in = anodePadSegments[1].out = anodePadSegments[1].p;
        anodePadSegments[1].corner = true;
        
        // 3rd point
        anodePadSegments[2].p.h = fStartingAnodePadPoint.h + 4.535;
        anodePadSegments[2].p.v = fStartingAnodePadPoint.v - 1.7;
        anodePadSegments[2].in = anodePadSegments[2].out = anodePadSegments[2].p;
        anodePadSegments[2].corner = true;
        
        // 4th point
        anodePadSegments[3].p.h = fStartingAnodePadPoint.h;
        anodePadSegments[3].p.v = fStartingAnodePadPoint.v - 1.7;
        anodePadSegments[3].in = anodePadSegments[3].out = anodePadSegments[3].p;
        anodePadSegments[3].corner = true;
        
        error = sAIPath->SetPathSegments(anodePad, 0, 4, anodePadSegments);
        if (error)
            goto error;
        
        error = sAIPath->SetPathClosed(anodePad, true);
        if (error)
            goto error;
        
        error = sAIPathStyle->GetCurrentPathStyle(&pathStyle, &pathStyleMap, &advStrokeParams);
        
        pathStyle.strokePaint = true;
        pathStyle.stroke.color.kind = kGrayColor;
        pathStyle.stroke.color.c.g.gray = kAIRealOne;
        pathStyle.stroke.width = kAIRealHalf;
        
        //led cathodePad
        error = sAIPath->SetPathSegmentCount(cathodePad, 4);
        AIRealPoint fStartingCathodePadPoint;
        fStartingCathodePadPoint.h = this->fStartingPoint.h + 2.834;
        fStartingCathodePadPoint.v = this->fStartingPoint.v - 2.834 - 9.071 + 1.7;
        // 1st point.
        cathodePadSegments[0].p.h = fStartingCathodePadPoint.h;
        cathodePadSegments[0].p.v = fStartingCathodePadPoint.v;
        cathodePadSegments[0].in = cathodePadSegments[0].out = cathodePadSegments[0].p;
        cathodePadSegments[0].corner = true;
        
        // 2nd point 9.071 4.535 pt
        cathodePadSegments[1].p.h = fStartingCathodePadPoint.h + 4.535;
        cathodePadSegments[1].p.v = fStartingCathodePadPoint.v;
        cathodePadSegments[1].in = cathodePadSegments[1].out = cathodePadSegments[1].p;
        cathodePadSegments[1].corner = true;
        
        // 3rd point
        cathodePadSegments[2].p.h = fStartingCathodePadPoint.h + 4.535;
        cathodePadSegments[2].p.v = fStartingCathodePadPoint.v - 1.7;
        cathodePadSegments[2].in = cathodePadSegments[2].out = cathodePadSegments[2].p;
        cathodePadSegments[2].corner = true;
        
        // 4th point
        cathodePadSegments[3].p.h = fStartingCathodePadPoint.h;
        cathodePadSegments[3].p.v = fStartingCathodePadPoint.v - 1.7;
        cathodePadSegments[3].in = cathodePadSegments[3].out = cathodePadSegments[3].p;
        cathodePadSegments[3].corner = true;
        
        error = sAIPath->SetPathSegments(cathodePad, 0, 4, cathodePadSegments);
        if (error)
            goto error;
        
        error = sAIPath->SetPathClosed(cathodePad, true);
        if (error)
            goto error;
        
        error = sAIPathStyle->GetCurrentPathStyle(&pathStyle, &pathStyleMap, &advStrokeParams);
        
        pathStyle.strokePaint = true;
        pathStyle.stroke.color.kind = kGrayColor;
        pathStyle.stroke.color.c.g.gray = kAIRealOne;
        pathStyle.stroke.width = kAIRealHalf;
    } else if(message->tool == this->fToolHandle[4]){
        //Tool is power pads
        AIArtHandle powerAnodePad;
        AIArtHandle powerCathodePad;
        
        // Create new art, we will fill it with points below.
        error = sAIArt->NewArt(kGroupArt, kPlaceAboveAll, NULL, &grpPower);
        if (error)
            goto error;
        
        error = sAIArt->NewArt(kPathArt, kPlaceInsideOnTop, grpPower, &powerAnodePad);
        if (error)
            goto error;
        sAIArt->SetArtName(powerAnodePad, ai::UnicodeString("anode"));
        
        error = sAIArt->NewArt(kPathArt, kPlaceInsideOnTop, grpPower, &powerCathodePad);
        if (error)
            goto error;
        sAIArt->SetArtName(powerCathodePad, ai::UnicodeString("cathode"));
        
        
        // Power anode pad
        error = sAIPath->SetPathSegmentCount(powerAnodePad, 4);
        AIRealPoint fStartingPowerAnodePadPoint;
        fStartingPowerAnodePadPoint.h = this->fStartingPoint.h;
        fStartingPowerAnodePadPoint.v = this->fStartingPoint.v;
        // 1st point.
        powerAnodePadSegments[0].p.h = fStartingPowerAnodePadPoint.h;
        powerAnodePadSegments[0].p.v = fStartingPowerAnodePadPoint.v;
        powerAnodePadSegments[0].in = powerAnodePadSegments[0].out = powerAnodePadSegments[0].p;
        powerAnodePadSegments[0].corner = true;
        
        // 2nd point 19.842519672540146pt == 7mm
        powerAnodePadSegments[1].p.h = fStartingPowerAnodePadPoint.h + 19.842519672540146;
        powerAnodePadSegments[1].p.v = fStartingPowerAnodePadPoint.v;
        powerAnodePadSegments[1].in = powerAnodePadSegments[1].out = powerAnodePadSegments[1].p;
        powerAnodePadSegments[1].corner = true;
        
        // 3rd point
        powerAnodePadSegments[2].p.h = fStartingPowerAnodePadPoint.h + 19.842519672540146;
        powerAnodePadSegments[2].p.v = fStartingPowerAnodePadPoint.v - 19.842519672540146;
        powerAnodePadSegments[2].in = powerAnodePadSegments[2].out = powerAnodePadSegments[2].p;
        powerAnodePadSegments[2].corner = true;
        
        // 4th point
        powerAnodePadSegments[3].p.h = fStartingPowerAnodePadPoint.h;
        powerAnodePadSegments[3].p.v = fStartingPowerAnodePadPoint.v - 19.842519672540146;
        powerAnodePadSegments[3].in = powerAnodePadSegments[3].out = powerAnodePadSegments[3].p;
        powerAnodePadSegments[3].corner = true;
        
        error = sAIPath->SetPathSegments(powerAnodePad, 0, 4, powerAnodePadSegments);
        if (error)
            goto error;
        
        error = sAIPath->SetPathClosed(powerAnodePad, true);
        if (error)
            goto error;
        
        error = sAIPathStyle->GetCurrentPathStyle(&pathStyle, &pathStyleMap, &advStrokeParams);
        
        pathStyle.strokePaint = false;
        pathStyle.fillPaint = true;
        pathStyle.fill.color.kind = kThreeColor;
        pathStyle.fill.color.c.rgb.red = kAIRealZero;
        pathStyle.fill.color.c.rgb.blue = kAIRealZero;
        pathStyle.fill.color.c.rgb.green = kAIRealZero;
        error = sAIPathStyle->SetPathStyle(powerAnodePad, &pathStyle);
        
        // Power cathode pad
        error = sAIPath->SetPathSegmentCount(powerCathodePad, 4);
        AIRealPoint fStartingPowerCathodePadPoint;
        //93.54330702768925pt == 33mm
        fStartingPowerCathodePadPoint.h = this->fStartingPoint.h + 93.54330702768925;
        fStartingPowerCathodePadPoint.v = this->fStartingPoint.v;
        // 1st point.
        powerCathodePadSegments[0].p.h = fStartingPowerCathodePadPoint.h;
        powerCathodePadSegments[0].p.v = fStartingPowerCathodePadPoint.v;
        powerCathodePadSegments[0].in = powerCathodePadSegments[0].out = powerCathodePadSegments[0].p;
        powerCathodePadSegments[0].corner = true;
        
        // 2nd point 19.842519672540146pt == 7mm
        powerCathodePadSegments[1].p.h = fStartingPowerCathodePadPoint.h + 19.842519672540146;
        powerCathodePadSegments[1].p.v = fStartingPowerCathodePadPoint.v;
        powerCathodePadSegments[1].in = powerCathodePadSegments[1].out = powerCathodePadSegments[1].p;
        powerCathodePadSegments[1].corner = true;
        
        // 3rd point
        powerCathodePadSegments[2].p.h = fStartingPowerCathodePadPoint.h + 19.842519672540146;
        powerCathodePadSegments[2].p.v = fStartingPowerCathodePadPoint.v - 19.842519672540146;
        powerCathodePadSegments[2].in = powerCathodePadSegments[2].out = powerCathodePadSegments[2].p;
        powerCathodePadSegments[2].corner = true;
        
        // 4th point
        powerCathodePadSegments[3].p.h = fStartingPowerCathodePadPoint.h;
        powerCathodePadSegments[3].p.v = fStartingPowerCathodePadPoint.v - 19.842519672540146;
        powerCathodePadSegments[3].in = powerCathodePadSegments[3].out = powerCathodePadSegments[3].p;
        powerCathodePadSegments[3].corner = true;
        
        error = sAIPath->SetPathSegments(powerCathodePad, 0, 4, powerCathodePadSegments);
        if (error)
            goto error;
        
        error = sAIPath->SetPathClosed(powerCathodePad, true);
        if (error)
            goto error;
        
        error = sAIPathStyle->GetCurrentPathStyle(&pathStyle, &pathStyleMap, &advStrokeParams);
        
        pathStyle.strokePaint = false;
        pathStyle.fillPaint = true;
        pathStyle.fill.color.kind = kThreeColor;
        pathStyle.fill.color.c.rgb.red = kAIRealZero;
        pathStyle.fill.color.c.rgb.blue = kAIRealZero;
        pathStyle.fill.color.c.rgb.green = kAIRealZero;
        error = sAIPathStyle->SetPathStyle(powerCathodePad, &pathStyle);
        
        
    }
    
    if (message->tool == this->fToolHandle[0]) {
        //Style led color---------------
        // fill and stroke with black; 0.5 point line
        
        pathStyle.fillPaint = true;
        pathStyle.fill.color.kind = kThreeColor;
        pathStyle.fill.color.c.rgb.red = 255.0;
        pathStyle.fill.color.c.rgb.blue = kAIRealZero;
        pathStyle.fill.color.c.rgb.green = kAIRealZero;
        
        error = sAIPathStyle->SetPathStyle(led, &pathStyle);
        
        //Add text
        try {
            // Add the new point text item to the layer.
            AITextOrientation orient = kHorizontalTextOrientation;
            AIRealPoint anchor;
            anchor.h = this->fStartingPoint.h + 3.65;
            anchor.v = this->fStartingPoint.v - 14.74 + 5.964;
            error = sAITextFrame->NewPointText(kPlaceInsideOnTop, grpLED, orient, anchor, &txt);
            aisdk::check_ai_error(error);
            sAIArt->SetArtName(txt, ai::UnicodeString("color"));
            
            // Set the contents of the text range.
            TextRangeRef range = NULL;
            error = sAITextFrame->GetATETextRange(txt, &range);
            aisdk::check_ai_error(error);
            ITextRange crange(range);
            crange.InsertAfter(ai::UnicodeString("R").as_ASUnicode().c_str());
            
            ICharFeatures feature;
            feature.SetFontSize(5.0);
            crange.SetLocalCharFeatures(feature);
        }
        catch (ai::Error& ex) {
            error = ex;
        }
        catch (ATE::Exception& ex) {
            error = ex.error;
        }
    }
    
    if (message->tool == this->fToolHandle[1]) {
        //Style led color---------------
        // fill and stroke with black; 0.5 point line
        
        pathStyle.fillPaint = true;
        pathStyle.fill.color.kind = kThreeColor;
        pathStyle.fill.color.c.rgb.red = kAIRealZero;
        pathStyle.fill.color.c.rgb.blue = kAIRealZero;
        pathStyle.fill.color.c.rgb.green = 255.0;
        
        error = sAIPathStyle->SetPathStyle(led, &pathStyle);
        
        //Add text
        try {
            // Add the new point text item to the layer.
            AITextOrientation orient = kHorizontalTextOrientation;
            AIRealPoint anchor;
            anchor.h = this->fStartingPoint.h + 3.65;
            anchor.v = this->fStartingPoint.v - 14.74 + 5.964;
            error = sAITextFrame->NewPointText(kPlaceInsideOnTop, grpLED, orient, anchor, &txt);
            aisdk::check_ai_error(error);
            sAIArt->SetArtName(txt, ai::UnicodeString("color"));
            
            // Set the contents of the text range.
            TextRangeRef range = NULL;
            error = sAITextFrame->GetATETextRange(txt, &range);
            aisdk::check_ai_error(error);
            ITextRange crange(range);
            crange.InsertAfter(ai::UnicodeString("G").as_ASUnicode().c_str());
            
            ICharFeatures feature;
            feature.SetFontSize(5.0);
            crange.SetLocalCharFeatures(feature);
        }
        catch (ai::Error& ex) {
            error = ex;
        }
        catch (ATE::Exception& ex) {
            error = ex.error;
        }
    }
    
    if (message->tool == this->fToolHandle[2]) {
        //Style led color---------------
        // fill and stroke with black; 0.5 point line
        
        pathStyle.fillPaint = true;
        pathStyle.fill.color.kind = kThreeColor;
        pathStyle.fill.color.c.rgb.red = kAIRealZero;
        pathStyle.fill.color.c.rgb.blue = 255.0;
        pathStyle.fill.color.c.rgb.green = kAIRealZero;
        
        error = sAIPathStyle->SetPathStyle(led, &pathStyle);
        
        //Add text
        try {
            // Add the new point text item to the layer.
            AITextOrientation orient = kHorizontalTextOrientation;
            AIRealPoint anchor;
            anchor.h = this->fStartingPoint.h + 3.65;
            anchor.v = this->fStartingPoint.v - 14.74 + 5.964;
            error = sAITextFrame->NewPointText(kPlaceInsideOnTop, grpLED, orient, anchor, &txt);
            aisdk::check_ai_error(error);
            sAIArt->SetArtName(txt, ai::UnicodeString("color"));
            
            // Set the contents of the text range.
            TextRangeRef range = NULL;
            error = sAITextFrame->GetATETextRange(txt, &range);
            aisdk::check_ai_error(error);
            ITextRange crange(range);
            crange.InsertAfter(ai::UnicodeString("B").as_ASUnicode().c_str());
            
            ICharFeatures feature;
            feature.SetFontSize(5.0);
            crange.SetLocalCharFeatures(feature);
        }
        catch (ai::Error& ex) {
            error = ex;
        }
        catch (ATE::Exception& ex) {
            error = ex.error;
        }
    }
    
    if (message->tool == this->fToolHandle[3]) {
        //Style led color---------------
        // fill and stroke with black; 0.5 point line
        
        pathStyle.fillPaint = true;
        pathStyle.fill.color.kind = kThreeColor;
        pathStyle.fill.color.c.rgb.red = 255.0;
        pathStyle.fill.color.c.rgb.blue = 255.0;
        pathStyle.fill.color.c.rgb.green = 255.0;
        
        error = sAIPathStyle->SetPathStyle(led, &pathStyle);
        
        //Add text
        try {
            // Add the new point text item to the layer.
            AITextOrientation orient = kHorizontalTextOrientation;
            AIRealPoint anchor;
            anchor.h = this->fStartingPoint.h + 3.65 - 0.688;
            anchor.v = this->fStartingPoint.v - 14.74 + 5.964;
            error = sAITextFrame->NewPointText(kPlaceInsideOnTop, grpLED, orient, anchor, &txt);
            aisdk::check_ai_error(error);
            sAIArt->SetArtName(txt, ai::UnicodeString("color"));
            
            // Set the contents of the text range.
            TextRangeRef range = NULL;
            error = sAITextFrame->GetATETextRange(txt, &range);
            aisdk::check_ai_error(error);
            ITextRange crange(range);
            crange.InsertAfter(ai::UnicodeString("W").as_ASUnicode().c_str());
            
            ICharFeatures feature;
            feature.SetFontSize(5.0);
            crange.SetLocalCharFeatures(feature);
        }
        catch (ai::Error& ex) {
            error = ex;
        }
        catch (ATE::Exception& ex) {
            error = ex.error;
        }
    }
    
    // Activate annotator.
    error = sAIAnnotator->SetAnnotatorActive(fAnnotatorHandle, true);
error:
    return error;
}

/*
*/
ASErr AgicToolPlugin::ToolMouseUp(AIToolMessage* message)
{
	ASErr error = kNoErr;

	// Deactivates annotator.
	error = sAIAnnotator->SetAnnotatorActive(fAnnotatorHandle, false);
	
	return error;
}

/*
*/
ASErr AgicToolPlugin::ToolMouseDrag(AIToolMessage* message)
{
	ASErr error = kNoErr;
/*	AIArtHandle path;
	AIPathSegment segments[8];
	AIReal pathAngle, arrowAngle;
	AIRealPoint arrowPt1, arrowPt2;
	AIPathStyle pathStyle;
	AIPathStyleMap pathStyleMap;
	AIDictionaryRef advStrokeParams = NULL;
	
	error = sAIUndo->UndoChanges( );

	this->fEndPoint = message->cursor;

	// Invalidate the old Annotation
	error = InvalidateRect(oldAnnotatorRect);
	aisdk::check_ai_error(error);
	
	// Create new art, we will fill it with points below.
	error = sAIArt->NewArt( kPathArt, kPlaceAboveAll, NULL, &path );
	if ( error )
		goto error;
		
	if (message->event->modifiers & aiEventModifiers_shiftKey)
	{
		short angle = (short)(abs((int)(sAIRealMath->RadianToDegree(sAIRealMath->AIRealPointAngle(&this->fStartingPoint, &this->fEndPoint)))));

		if (angle < 45 || angle > 135)
		{
			this->fEndPoint.v = this->fStartingPoint.v;
		}
		else
		{
			this->fEndPoint.h = this->fStartingPoint.h;
		}
	}

	
	if ( message->tool == this->fToolHandle[0] ) {
		//	HEAD ARROW
		// head arrow has 5 points	
		error = sAIPath->SetPathSegmentCount( path, 5 );		

		// beginning (and end) point. This is butt end of arrow
		segments[0].p.h = this->fStartingPoint.h;
		segments[0].p.v = this->fStartingPoint.v;
		segments[0].in = segments[0].out = segments[0].p;
		segments[0].corner = true;

		// arrow head point
		segments[1].p.h = this->fEndPoint.h;
		segments[1].p.v = this->fEndPoint.v;
		segments[1].in = segments[1].out = segments[1].p;
		segments[1].corner = true;

		// angle created by line segment
		pathAngle = sAIRealMath->AIRealPointAngle( &this->fEndPoint, &this->fStartingPoint );
		arrowAngle = sAIRealMath->DegreeToRadian( _ShortToAIReal(20) );

		sAIRealMath->AIRealPointLengthAngle( _ShortToAIReal(10), pathAngle + arrowAngle, &arrowPt1 );
		sAIRealMath->AIRealPointLengthAngle( _ShortToAIReal(10), pathAngle - arrowAngle, &arrowPt2 );

		segments[2].p.h = this->fEndPoint.h + arrowPt1.h;
		segments[2].p.v = this->fEndPoint.v + arrowPt1.v;
		segments[2].in = segments[2].out = segments[2].p;
		segments[2].corner = true;

		segments[3].p.h = this->fEndPoint.h + arrowPt2.h;
		segments[3].p.v = this->fEndPoint.v + arrowPt2.v;
		segments[3].in = segments[3].out = segments[3].p;
		segments[3].corner = true;		

		segments[4].p.h = this->fEndPoint.h;
		segments[4].p.v = this->fEndPoint.v;
		segments[4].in = segments[4].out = segments[4].p;
		segments[4].corner = true;		

		error = sAIPath->SetPathSegments( path, 0, 5, segments );
		if ( error )
			goto error;
			
		}

	else if ( message->tool == this->fToolHandle[1] ) {
		
		//	TAIL ARROW
		// tail arrow has 5 points	
		error = sAIPath->SetPathSegmentCount( path, 5 );		

		// beginning point. This is point of arrow
		segments[0].p.h = this->fStartingPoint.h;
		segments[0].p.v = this->fStartingPoint.v;
		segments[0].in = segments[0].out = segments[0].p;
		segments[0].corner = true;

		// angle created by line segment
		pathAngle = sAIRealMath->AIRealPointAngle( &this->fEndPoint, &this->fStartingPoint );
		arrowAngle = sAIRealMath->DegreeToRadian( _ShortToAIReal(20) );
		sAIRealMath->AIRealPointLengthAngle( _ShortToAIReal(10), pathAngle + arrowAngle, &arrowPt1 );
		sAIRealMath->AIRealPointLengthAngle( _ShortToAIReal(10), pathAngle - arrowAngle, &arrowPt2 );

		// arrow point 1
		segments[1].p.h = this->fStartingPoint.h - arrowPt1.h;
		segments[1].p.v = this->fStartingPoint.v - arrowPt1.v;
		segments[1].in = segments[1].out = segments[1].p;
		segments[1].corner = true;

		// arrow point 2
		segments[2].p.h = this->fStartingPoint.h - arrowPt2.h;
		segments[2].p.v = this->fStartingPoint.v - arrowPt2.v;
		segments[2].in = segments[2].out = segments[2].p;
		segments[2].corner = true;
		
		// beginning point. This is point of arrow
		segments[3].p.h = this->fStartingPoint.h;
		segments[3].p.v = this->fStartingPoint.v;
		segments[3].in = segments[3].out = segments[3].p;
		segments[3].corner = true;

		// end point. This is the butt end of arrow
		segments[4].p.h = this->fEndPoint.h;
		segments[4].p.v = this->fEndPoint.v;
		segments[4].in = segments[4].out = segments[4].p;
		segments[4].corner = true;

		error = sAIPath->SetPathSegments( path, 0, 5, segments );
		if ( error )
			goto error;
		}
		
		else if ( message->tool == this->fToolHandle[2] ) {
		//	BOTH ENDS ARROW
		// tail arrow has 8 points	
		error = sAIPath->SetPathSegmentCount( path, 8 );		

		// beginning point. This is point of arrow
		segments[0].p.h = this->fStartingPoint.h;
		segments[0].p.v = this->fStartingPoint.v;
		segments[0].in = segments[0].out = segments[0].p;
		segments[0].corner = true;

		// angle created by line segment
		pathAngle = sAIRealMath->AIRealPointAngle( &this->fEndPoint, &this->fStartingPoint );
		arrowAngle = sAIRealMath->DegreeToRadian( _ShortToAIReal(20) );
		sAIRealMath->AIRealPointLengthAngle( _ShortToAIReal(10), pathAngle + arrowAngle, &arrowPt1 );
		sAIRealMath->AIRealPointLengthAngle( _ShortToAIReal(10), pathAngle - arrowAngle, &arrowPt2 );

		// arrow point 1
		segments[1].p.h = this->fStartingPoint.h - arrowPt1.h;
		segments[1].p.v = this->fStartingPoint.v - arrowPt1.v;
		segments[1].in = segments[1].out = segments[1].p;
		segments[1].corner = true;

		// arrow point 2
		segments[2].p.h = this->fStartingPoint.h - arrowPt2.h;
		segments[2].p.v = this->fStartingPoint.v - arrowPt2.v;
		segments[2].in = segments[2].out = segments[2].p;
		segments[2].corner = true;
		
		// beginning point. This is point of arrow
		segments[3].p.h = this->fStartingPoint.h;
		segments[3].p.v = this->fStartingPoint.v;
		segments[3].in = segments[3].out = segments[3].p;
		segments[3].corner = true;

		// end point. This is the butt end of arrow
		segments[4].p.h = this->fEndPoint.h;
		segments[4].p.v = this->fEndPoint.v;
		segments[4].in = segments[4].out = segments[4].p;
		segments[4].corner = true;

		// arrow point 3
		segments[5].p.h = this->fEndPoint.h + arrowPt1.h;
		segments[5].p.v = this->fEndPoint.v + arrowPt1.v;
		segments[5].in = segments[5].out = segments[5].p;
		segments[5].corner = true;
		
		// arrow point 4
		segments[6].p.h = this->fEndPoint.h + arrowPt2.h;
		segments[6].p.v = this->fEndPoint.v + arrowPt2.v;
		segments[6].in = segments[6].out = segments[6].p;
		segments[6].corner = true;		
		
		// end point. This is the butt end of arrow
		segments[7].p.h = this->fEndPoint.h;
		segments[7].p.v = this->fEndPoint.v;
		segments[7].in = segments[7].out = segments[7].p;
		segments[7].corner = true;
		
		error = sAIPath->SetPathSegments( path, 0, 8, segments );
		if ( error )
			goto error;
		}
		
	else if ( message->tool == this->fToolHandle[3] ) {
		//	STRAIGHT LINE
		error = sAIPath->SetPathSegmentCount( path, 2 );		

		// beginning point.
		segments[0].p.h = this->fStartingPoint.h;
		segments[0].p.v = this->fStartingPoint.v;
		segments[0].in = segments[0].out = segments[0].p;
		segments[0].corner = true;

		// end point
		segments[1].p.h = this->fEndPoint.h;
		segments[1].p.v = this->fEndPoint.v;
		segments[1].in = segments[1].out = segments[1].p;
		segments[1].corner = true;
		
		error = sAIPath->SetPathSegments( path, 0, 2, segments );
		if ( error )
			goto error;

	}

	error = sAIPath->SetPathClosed( path, true );
	if ( error )
		goto error;
		
	// fill and stroke with black; 1 point line
	error = sAIPathStyle->GetCurrentPathStyle( &pathStyle, &pathStyleMap, &advStrokeParams );
	pathStyle.fillPaint = true;
	pathStyle.fill.color.kind = kGrayColor;
	pathStyle.fill.color.c.g.gray = kAIRealOne;
	
	pathStyle.strokePaint = true;
	pathStyle.stroke.color.kind = kGrayColor;
	pathStyle.stroke.color.c.g.gray = kAIRealOne;
	pathStyle.stroke.width = kAIRealOne;
	error = sAIPathStyle->SetPathStyle( path, &pathStyle );

error:*/
	return error;
}

/*
*/
ASErr AgicToolPlugin::AddAnnotator(SPInterfaceMessage *message)
{
	ASErr result = kNoErr;
	try
	{
		result = sAIAnnotator->AddAnnotator(message->d.self, "MultiArrowTool Annotator", &fAnnotatorHandle);
		aisdk::check_ai_error(result);

		result = sAIAnnotator->SetAnnotatorActive(fAnnotatorHandle, false);
		aisdk::check_ai_error(result);
	}
	catch (ai::Error& ex)
	{
		result = ex;
	}
	catch(...)
	{
		result = kCantHappenErr;
	}
	return result;
}

/*
*/
ASErr AgicToolPlugin::DrawAnnotator(AIAnnotatorMessage* message)
{
	ASErr result = kNoErr;
	try
	{
		// Get the string to display in annotator.
		ai::UnicodeString pointStr;
		result = this->GetPointString(fEndPoint, pointStr);
		aisdk::check_ai_error(result);

		AIPoint annotatorPoint;
		result = sAIDocumentView->ArtworkPointToViewPoint(NULL, &fEndPoint, &annotatorPoint);

		// Move 5 points right and 5 points up.
		annotatorPoint.h += 5;
		annotatorPoint.v -= 5;

		// Find cursor bound rect.
		AIRect annotatorRect;
		result = sAIAnnotatorDrawer->GetTextBounds(message->drawer, pointStr, &annotatorPoint,false, annotatorRect);
		aisdk::check_ai_error(result);

		// Draw a filled rectangle, the following R, G and B values combined makes light yellow.
		unsigned short red = 65000;
		unsigned short green = 65000;
		unsigned short blue = 40000;
		AIRGBColor yellowFill = {red, green, blue};
		sAIAnnotatorDrawer->SetColor(message->drawer, yellowFill);
		result = sAIAnnotatorDrawer->DrawRect(message->drawer, annotatorRect, true);
		aisdk::check_ai_error(result);

		// Draw black outline, 0 for R, G and B makes black.
		unsigned short black = 0;
		AIRGBColor blackFill = {black, black, black};
		sAIAnnotatorDrawer->SetColor(message->drawer, blackFill);
		sAIAnnotatorDrawer->SetLineWidth(message->drawer, 0.5);
		result = sAIAnnotatorDrawer->DrawRect(message->drawer, annotatorRect, false);
		aisdk::check_ai_error(result);

		// Draw cursor text.
		result = sAIAnnotatorDrawer->SetFontPreset(message->drawer, kAIAFSmall);
		aisdk::check_ai_error(result);

		result = sAIAnnotatorDrawer->DrawTextAligned(message->drawer, pointStr, kAICenter, kAIMiddle, annotatorRect);
		aisdk::check_ai_error(result);

		// Save old rect
		oldAnnotatorRect = annotatorRect;
	}
	catch (ai::Error& ex)
	{
		result = ex;
	}
	return result;
}

ASErr AgicToolPlugin::InvalAnnotator(AIAnnotatorMessage *message)
{
	ASErr result = sAIAnnotator->InvalAnnotationRect(NULL, &oldAnnotatorRect);

	return result;
}

ASErr AgicToolPlugin::InvalidateRect(const AIRect& invalRect)
{
	ASErr result = kNoErr;
	try {		
		// Invalidate the rect bounds so it is redrawn.
		result = sAIAnnotator->InvalAnnotationRect(NULL, &invalRect);
		aisdk::check_ai_error(result);
	}
	catch (ai::Error& ex) {
		result = ex;
	}
	return result;
}

/*
*/
ASErr AgicToolPlugin::InvalidateRect(const AIRealRect& invalRealRect)
{
	ASErr result = kNoErr;
	try {		
		// invalRealRect is in artwork coordinates, convert to view 
		// coordinates for AIAnnotatorSuite::InvalAnnotationRect.
		AIRect invalRect;
		result = this->ArtworkBoundsToViewBounds(invalRealRect, invalRect);
		aisdk::check_ai_error(result);

		// Invalidate the rect bounds so it is redrawn.
		result = sAIAnnotator->InvalAnnotationRect(NULL, &invalRect);
		aisdk::check_ai_error(result);
	}
	catch (ai::Error& ex) {
		result = ex;
	}
	return result;
}

/*
*/
ASErr AgicToolPlugin::ArtworkBoundsToViewBounds(const AIRealRect& artworkBounds, AIRect& viewBounds)
{
	ASErr result = kNoErr;
	try {		
		AIRealPoint tlArt, brArt;
		tlArt.h = artworkBounds.left;
		tlArt.v = artworkBounds.top;
		brArt.h = artworkBounds.right;
		brArt.v = artworkBounds.bottom;

		// Convert artwork coordinates to view coordinates.
		AIPoint tlView, brView;
		result = sAIDocumentView->ArtworkPointToViewPoint(NULL, &tlArt, &tlView);
		aisdk::check_ai_error(result);
		result = sAIDocumentView->ArtworkPointToViewPoint(NULL, &brArt, &brView);
		aisdk::check_ai_error(result);

		viewBounds.left = tlView.h;
		viewBounds.top = tlView.v;
		viewBounds.right = brView.h;
		viewBounds.bottom = brView.v;
	}
	catch (ai::Error& ex) {
		result = ex;
	}
	return result;
}

/*
*/
ASErr AgicToolPlugin::GetPointString(const AIRealPoint& point, ai::UnicodeString& pointStr)
{
	ASErr result = kNoErr;
	try
	{	
		ASInt32 precision = 2;
		ai::UnicodeString horiz, vert;
		ai::NumberFormat numFormat;

		horiz = numFormat.toString((float) point.h, precision, horiz);
		vert = numFormat.toString((float) -point.v, precision, vert);

		pointStr.append(ai::UnicodeString("h: ")
				.append(horiz)
				.append(ai::UnicodeString(", v: ")
				.append(vert)));
	}
	catch (ai::Error& ex)
	{
		result = ex;
	}
	return result;
}

ASErr AgicToolPlugin::PostStartupPlugin()
{
    AIErr result = kNoErr;
    result = sAIUser->CreateCursorResourceMgr(fPluginRef,&fResourceManagerHandle);
    return result;
}

// End MultiArrowToolPlugin.cpp
