//========================================================================================
//  
//  $File: //ai_stream/rel_20_0/devtech/sdk/public/samplecode/MultiArrowTool/Source/MultiArrowToolID.h $
//
//  $Revision: #1 $
//
//  Copyright 1987 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __MULTIARROWTOOLID_H__
#define __MULTIARROWTOOLID_H__

#define kMultiArrowToolPluginName	"MultiArrowTool"

#define kCursorID				16000

#define kHeadArrowToolIconResID			19002
#define kTailArrowToolIconResID			19006
#define kBothEndsArrowToolIconResID		19000
#define kStraightLineToolIconResID		19004

#define kHeadArrowToolIconDarkResID		19003
#define kTailArrowToolIconDarkResID		19007
#define kBothEndsArrowToolIconDarkResID	19001
#define kStraightLineToolIconDarkResID	19005

#endif // End MultiArrowToolID.h
